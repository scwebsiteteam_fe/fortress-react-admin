import React from 'react';
import FontIcon from 'material-ui/FontIcon';
import { orange600, cyan600, purple600 } from 'material-ui/styles/colors';

const data = {
  menus: [
    { id: 'dashboard', text: 'DashBoard', icon: <FontIcon className="material-icons">assessment</FontIcon>, url: '/', index: 0 },
    { id: 'system-admin-page', text: 'System Admin', icon: <FontIcon className="material-icons">settings</FontIcon>, url: '/system-admin', index: 1 },
  ],
  dashBoardPage: {
    recentProducts: [
      { id: 1, title: 'Samsung Galaxy S7', text: 'Samsung Galaxy S7 G930F 32GB Factory Unlocked GSM Smartphone International Version (Gold)' },
      { id: 2, title: 'Sony Xperia XZ', text: 'Sony Xperia XZ - Unlocked Smartphone - 32GB - Platinum (US Warranty)' },
      { id: 3, title: 'ACER R240HY 23.8-Inch', text: 'Acer R240HY bidx 23.8-Inch IPS HDMI DVI VGA (1920 x 1080) Widescreen Monitor' },
      { id: 4, title: 'Dell 15.6-Inch Gaming Laptop', text: 'Dell 15.6-Inch Gaming Laptop (6th Gen Intel Quad-Core i5-6300HQ Processor up to 3.2GHz, 8GB DDR3, 256GB SSD, Nvidia GeForce GTX 960M, Windows 10)' },
    ],
    monthlySales: [
      { name: 'Jan', uv: 1800 },
      { name: 'Feb', uv: 2000 },
      { name: 'Mar', uv: 2780 },
      { name: 'Apr', uv: 2000 },
      { name: 'May', uv: 3000 },
      { name: 'Jun', uv: 3700 },
    ],
    newOrders: [
      { name: 'Jan', 'New Orders': 3908 },
      { name: 'Feb', 'New Orders': 3490 },
      { name: 'Mar', 'New Orders': 4800 },
      { name: 'Apr', 'New Orders': 2400 },
      { name: 'May', 'New Orders': 4500 },
      { name: 'Jun', 'New Orders': 7890 },
    ],
    browserUsage: [
      { name: 'Chrome', value: 1200, color: orange600, icon: <FontIcon className="material-icons">chevron_right</FontIcon> },
      { name: 'Firefox', value: 700, color: purple600, icon: <FontIcon className="material-icons">chevron_right</FontIcon> },
      { name: 'Safari', value: 150, color: cyan600, icon: <FontIcon className="material-icons">chevron_right</FontIcon> },
    ],
  },
};

export default data;

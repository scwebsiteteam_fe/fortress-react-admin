/*
 * SystemAdminPage Messages
 *
 * This contains all the text for the SystemAdminPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.SystemAdminPage.header',
    defaultMessage: 'This is SystemAdminPage container !',
  },
});

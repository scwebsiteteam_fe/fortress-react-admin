import { createSelector } from 'reselect';

/**
 * Direct selector to the systemAdminPage state domain
 */
const selectSystemAdminPageDomain = () => (state) => state.get('systemAdminPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by SystemAdminPage
 */

const makeSelectSystemAdminPage = () => createSelector(
  selectSystemAdminPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectSystemAdminPage;
export {
  selectSystemAdminPageDomain,
};


import { fromJS } from 'immutable';
import systemAdminPageReducer from '../reducer';

describe('systemAdminPageReducer', () => {
  it('returns the initial state', () => {
    expect(systemAdminPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
